import sympy
import numpy as np
import matplotlib.pyplot as plt
import os
import xlrd
import xlwt
from scipy import signal
from scipy.optimize import curve_fit

workbook=xlrd.open_workbook('xps-data.xls')
first_sheet=workbook.sheet_by_index(0)
xdata1a=first_sheet.col_slice(colx=0, start_rowx=2,end_rowx=5003)
ydata1a=first_sheet.col_slice(colx=1, start_rowx=2,end_rowx=5003)
xdata1b=first_sheet.col_slice(colx=2, start_rowx=2,end_rowx=5003)
ydata1b=first_sheet.col_slice(colx=3, start_rowx=2,end_rowx=5003)
xdata2=first_sheet.col_slice(colx=4, start_rowx=2,end_rowx=5003)
ydata2=first_sheet.col_slice(colx=5, start_rowx=2,end_rowx=5003)

xdata1a_abs=[]
ydata1a_abs=[]
xdata1b_abs=[]
ydata1b_abs=[]
xdata2_abs=[]
ydata2_abs=[]
ypeaks1b=[]
xpeaks1b=[]
ypeaks1a=[]
xpeaks1a=[]
ypeaks2=[]
xpeaks2=[]
for xval,yval in zip(xdata1a,ydata1a):
	xdata1a_abs.append(xval.value)
	ydata1a_abs.append(yval.value)

for xval,yval in zip(xdata1b,ydata1b):
	xdata1b_abs.append(xval.value)
	ydata1b_abs.append(yval.value)

for xval,yval in zip(xdata2,ydata2):
        xdata2_abs.append(xval.value)
        ydata2_abs.append(yval.value)

peaks1b = signal.find_peaks_cwt(ydata1b_abs, np.arange(1,10),min_snr=3,noise_perc=6)
for element in peaks1b:
	ypeaks1b.append(ydata1b_abs[element])
	xpeaks1b.append(xdata1b_abs[element])

def func(x,m,b):
	return m*x+b

params= curve_fit(func,xpeaks1b,ypeaks1b)
m,b=params[0]
print("m-",m,"b-",b)
#plt.plot(m*xpeaks1b+b)
plt.plot(xdata1b_abs,ydata1b_abs)
plt.plot(xpeaks1b,ypeaks1b,'ro')
plt.title('Problem 1b Data', color="red")
plt.grid(True)
plt.xlabel('Binding energy (eV)')
plt.ylabel('Intensity (counts/second)')
plt.savefig('1B')
plt.clf()

peaks1a = signal.find_peaks_cwt(ydata1a_abs, np.arange(1,10),min_snr=3,noise_perc=6)
for element in peaks1a:
        ypeaks1a.append(ydata1a_abs[element])
        xpeaks1a.append(xdata1a_abs[element])
plt.plot(xpeaks1a,ypeaks1a,'ro')
plt.plot(xdata1a_abs,ydata1a_abs)
plt.title('Problem 1a Data', color="red")
plt.grid(True)
plt.xlabel('Binding energy (eV)')
plt.ylabel('Intensity (counts/second)')
plt.savefig('1a')
plt.clf()

peaks2 = signal.find_peaks_cwt(ydata2_abs, np.arange(1,20),min_snr=3,noise_perc=6)
for element in peaks2:
        ypeaks2.append(ydata2_abs[element])
        xpeaks2.append(xdata2_abs[element])
plt.plot(xdata2_abs,ydata2_abs)
plt.plot(xpeaks2,ypeaks2,'ro')
plt.title('Problem 2 Data', color="red")
plt.grid(True)
plt.xlabel('Binding energy (eV)')
plt.ylabel('Intensity (counts/second)')
plt.savefig('2')
plt.clf()

