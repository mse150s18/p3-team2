#**p3-team2 Code Information**
##Instructions
- First, you need to put your file location so that python opens your excel data file
- Then python will go into your excel data file and find what columns to slice for the data
- You will define the titles of your graphs with the plt.title command
- You will want to define the axis by using the plt.xlabel and plt.ylabel commands
- Run the code and the graphs should pop up
##Major Assumptions
* Must be analyzing an excel file instead of a text file.
* The format of the excel file must match the format of the excel file the script was based off of.
##Expected Output
* Python will graph the actual peaks of each graph
* part 2 of option one outcome
- The graphs will be deleted as soon as you close them
- Each graph will be titled and labeled on the x and y axis

