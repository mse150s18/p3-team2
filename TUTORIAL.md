#**TUTORIAL.md**


##**Kailee Jones**
* **data=np.loadtxt(f) #** this line tells python how to define the variable data in the spectra file so that in can be defined this way elseway in the following code
* **workbook.close() #** here, python is being told to call the close function from the workbook object which should open from the code close eliminating the work later to do so but can still be visible in file
* **plt.title('Problem 1b Data', color="red") #** this line of code tells python that in the 1b plotted data  the graph the title specified, in this case the title is 'Problem 1b Data'
* **xdata1a=first_sheet.col_slice(colx=1, start_rowx=2,end_rowx=5003) #** in this line of code python is told to read the data points in the excel file rather than the cells so that we have correct data points in the graph thus it is reading all cell in the exel file and only taking the numbers to put into the graph
* **plt.savefig('2') #** in this line of code python is supposed to save the ploted figure '2', this allows one to go back to the figure and check the accuracy of the code
* **xdata1a_abs.append(xval.value) #** this line of code tells python to collect the proper x value for that graph, it does this by sorting through the excel file and only finding the points related to the defined data
* **plt.grid(True) #** this line of code shows python the direction to take the x and y axis values and assemble them into a graph, thus it is scanning through the data to obtain the scale of best fit
* **plt.ylabel('Intensity (counts/second)') #** by stating plot it tells python to print and the ylabel and from that it goes to the ylable and  prints the following message 'Intensity (counts/second)'
* **import matplotlib.pyplot as plt #** this line of code allows python to import the matplot data into python so that there is data to graph in the code following this line, it does this by converting the matplotlib.pyplot data into a readable python document
* **first_sheet=workbook.sheet_by_index(0) #** this line of code allows python to go to the workbook in the first sheet to retrieve and store the data needed for the lines of codes following this line

##**Tyler Hamer**
* **somevariable = signal.find_peaks_cwt() -** this a command from the module scipy. This finds the peaks in a data set. Inside the parentheses is where you will put the parameters for how sensitive the peak detect is.
* **varialbe.append(xval.value) -** When running this in a for loop, python takes data from a variable and adds it to an empty object with the .append, the .value only keeps the number when appending it to the empty object. This was helpful when slicing the excel file in project 3 and adding the data to a variable.
* **xdata1a=first_sheet.col_slice(colx=0, start_rowx=2,end_rowx=5003) -** This is the code I used to slice the excell spreadsheet in project 3. The code tells python to slice the excell spread sheet at the specified column, row, and add call it xdata1a
* **xdata1a_abs=[] -** This defines a empty variable by using the empty square brackets. This is something that you could use the .append to add data from another source into this variable.
* **from scipy import signal -** When finding the peaks with scipy. This is how you load the module at the beginning of your python script. This will allow the signal.find_peaks_cwt() to work.
* **print(something) -** The print command will print whatever you put in the parentheses. You can print entire variables, or print only text if you put appostrophes around the text.
* **from PIL import Image -** This imports the Image module from PIL and allows you to do some image editing like we did in project one stitching the photos together.
* **Image.open(image_file) -** This is the command from the Image module, and opens a specified image file with python.
* **im_resized = im.resize((128, 128) -** The im.resize, resized the image that we were using to 128,128 pixels using the Image from PIL.
* **team_logo.save('team_logo.jpg') -** the .save saved our team logo and named it team_logo.jpg because it was specified in the parentheses.

##**Kevin Saythavy**
* **for x in glob.glob('xps-data/*.png')) -** in this line of code we define all files inside the directory which is defined as glob.globwhich takes all files within the defined folder, in this case xps-data. Afterwards the os.remove is what clears any created images from previous scripts that end in .png with the '*' being a wildcard that allows any filename to go before the .png.
* **workbook=xlrd.openworkbook('xps-data.xls') -** this defines the word 'workbook' as a command that runs the openworkbook function from the xlrd package. We then define that we want the file named 'xps-data.xls' opened.
* **plt.clf() -** when a figure is created in python, the image is saved, and this creates conflict when plotting another set of data later on, thus using clf from the matplotlib.pyplot package clears the figure. We use this at the end of each plotting script
* **import x as y -** for this x would be any package, say matplotlib.pyplot and y would be whatever you wanted to name the package for ease of access later on. An example for y would be plt, to call the matplotlib.pyplot package by typing in plt. 
* **first_sheet=workbook.sheet_by_index(0) -** this defines the variable first_sheet as the function that opens workbook.sheet_by_index and then asks for number (0) which pulls the first page from the workbook.
* **xdata1a=first_sheet.col_slice(colx=0, start_rowx=2, end_rowx=5003) -** this again, defines a function as xdata1a and it defines it as the col_slice function on the first_sheet that was defined before. This looks at only one column, in this case the first one as colx=0, then starts in the 3rd row as defined by start_rowx=2. It then ends in the 5004th row.
* **plt.xlabel('Binding energy (eV)') -** when plotting, the function xlabel is pulled from package plt, which in this case is matplotlib.pyplot. This puts the x axis label as the defined ('Binding Energy (ev)')
* **plt.savefig('1B') -** after plotting, the figure that was created through previous lines will be saved as a file named 1B. This uses the savefig function from the defined plt package.
* **plt.plot(xdata1b_abs, ydata1b_abs) -** calls the plot function from plt and plots variabls xdata1b_abs and ydata1b_abs as x and y values respectively.
* **for xval,yval in zip(xdata1b,ydata1b): -** this is a for loop, which runs a function for the defined set zip(xdata1b, ydata1b) any code that is indented correctly and is preceded by this command, will run for the 2 variables called upon.

##**Sam**
* **img = plt.imread('grains.py') -** This line runs the function 'imread' from the plt package with the argument of 'grains.py' and assigns the value of that to img.
* **gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY) -** This line of code creates an object called "gray" which is the same as the object img but in greyscale. It uses the cvtColor function of the cv2 package with img being the image to be manipulated and BGR2GRAY being the argument which tells the function the image is being converted to greyscale.
* **gray = cv2.GaussianBlur(gray, (5,5), 0) -** What this line of code does is take the object 'gray' and blurs it with a Gaussian smoothing function. This helps remove noise and is the first step in pre-processing
* **import cv2 -**This is on of the lines which starts our grains.py code. It calls to import the cv2 package which is full of tools that can be used for image processing.
* **workbook=xlrd.open_workbook('xps-data.xls') -** This line of code creates the object 'workbook' and retrieves the data from the specified file using the xlrd python package.
* **plt.title('Problem 2 Data', color="red") -** This specific line uses the plt package to assign a label to a graph. The title is the first argument and the second argument defines the color.  
* **import xlwt -** This imports the python library for writing data and formatting information with older excel files
* **import xlrd -** This line imports another python library similar to xlwt, only with more emphasis on extracting the data as opposed to writing it.
* **plt.xlabel('argument') -** This line uses the xlabel argument of the plt package to assign a label (the argument) to the x-axis of the plot currently being manipulated.
* **plt.ylabel('argument') -** This line of code does the exact same thing as the previous line, only with the y-axis. Using the plt package and associated ylabel function to assign a string argument to our plot. 

##**Erin**
* **import numpy #** in this line of code, we are importing a package of code into python that helps interpret number data that you have that you want to use python to organize
* **import sympy #** in this line of code, we are importing a package of code into python that helps python interpret number data into symbols rather than numbers ex: instead of square root of 8 being a continous number it will interpret it as 2 multiplied by the square root of 2
* **git clone SSH #** this line of code clones data from an online source (in this case, Bitbucket) and allows us to manipulate it through the terminal on our computer
* **import os #** this line of code is for importing a package of code in python that allow the programmer to interface with the underlying operating system that python is running on
* **plt.clf() #** this command clears the figure that was brought up but does not close the figure window so that you can pull up another plot
* **from scipy.optimize import curve_fit #** in this line of code, python is using least-squares minimization to curve fit, where you have a paramaterized model function that adjusts the numerical values for the model so this it most closely matches some data
* **for element in peaks 1b: #** in this line of code, python is finding the highest number in the array of data that it has for 1b
* **def func(x,m,b): #** this line of code is defining the function for the variables x, m, and b, telling python that these are the variables in the task that we are about to carry out
* **peaks1b=signal.find_peaks_cwt(variables and data plot points) #** this line of code is telling python to find the peaks in a 1-D array
* **plt.xlabel('Binding energy (ev)') #** in this line of code, we are telling python to label the x-axis with the title and also what units we are measuring the energy at
